Here are the exercises I did while finishing the following course on pluralsight.

The exercises are not complete. 

I didn't manage to deploy my servlet as described in the chapter "Packaging Servlets for Deployment".

# [Java EE: Programming Servlets](https://app.pluralsight.com/library/courses/java-ee-programming-servlets/table-of-contents)
##### by Sekhar Srinivasan

### Java servlets are the first step to understanding web programming using Java, and this course will show you the basics of servlet architecture and applying filters to more complex issues like debugging and deployment.

Web application development can be made easier by using Java servlets. In this course, Java EE: Programming Servlets, you will begin with a complet overview of servlet architecture and lifecycle. First, you'll see the configuration of a Tomcat webserver in Eclipse and you'll learn how to read the request and response headers. Next, you'll learn how filters are applied to servlets and see many details about tracking session data, web annotations, and globalizing servlets. Finally, you'll go over asynchronous programming in servlets, debugging, packing, and deployment of servlets. By the end of this course, you will have a much more complete understanding of how web development using Java servlets works. Software required: Tomcat and Eclipse.

#### Contents

1. `Course Overview - - - - - - - - - - - - - - - - 01:21`
1. `Introduction- - - - - - - - - - - - - - - - - - 22:07`
1. `Building and Deploying Your First Servlet - - - 11:02`
1. `Working with Form and Query String Data - - - - 26:25`
1. `Handling HTTP Requests and Responses- - - - - - 33:32`
1. `Intercepting HTTP Request with Filters- - - - - 23:25`
1. `Handling Exceptions in Servlets - - - - - - - - 08:23`
1. `Tracking Session Data - - - - - - - - - - - - - 45:54`
1. `Receiving and Processing File Uploads - - - - - 10:51`
1. `Packaging Servlets for Deployment - - - - - - - 13:17`
1. `Debugging Servlets- - - - - - - - - - - - - - - 13:47`
1. `Globalizing Servlets- - - - - - - - - - - - - - 25:04`
1. `Providing Servlet Metadata Using Annotations- - 19:04`
1. `Understanding Asynchronous Servlet Processing - 12:13`
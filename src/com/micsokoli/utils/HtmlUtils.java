package com.micsokoli.utils;

public class HtmlUtils {
	public static String wrapInTag(String tag, String content) {
		return "<" + tag + ">" +
				content +
				"</" + tag + ">";
	}
}
package com.micsokoli.demos;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Register
 */
@WebServlet("/Register")
public class Register extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Register() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String sex = request.getParameter("sex");
		String[] hobbies = request.getParameterValues("hobbies");
		String country = request.getParameter("country");
		String[] languages = request.getParameterValues("languages");

		response.setContentType("text/html");

		PrintWriter out = response.getWriter();
		out.println("<p>Username: " + username + "</p>");
		out.println("<p>Password: " + password + "</p>");
		out.println("<p>Sex: " + sex + "</p>");
		out.println("<p>Hobbies: ");
		out.println("<ul>");
		for (String hobby : hobbies) {
			out.println("<li>" + hobby + "</li>");
		}
		out.println("</ul>");
		out.println("</p>");
		out.println("<p>Country: " + country + "</p>");
		out.println("<p>Languages: ");
		out.println("<ul>");
		for (String language : languages) {
			out.println("<li>" + language + "</li>");
		}
		out.println("</ul>");
		out.println("</p>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String sex = request.getParameter("sex");
		String[] hobbies = request.getParameterValues("hobbies");
		String country = request.getParameter("country");
		String[] languages = request.getParameterValues("languages");

		response.setContentType("text/html");

		PrintWriter out = response.getWriter();
		out.println("<p>Username: " + username + "</p>");
		out.println("<p>Password: " + password + "</p>");
		out.println("<p>Sex: " + sex + "</p>");
		out.println("<p>Hobbies: ");
		out.println("<ul>");
		for (String hobby : hobbies) {
			out.println("<li>" + hobby + "</li>");
		}
		out.println("</ul>");
		out.println("</p>");
		out.println("<p>Country: " + country + "</p>");
		out.println("<p>Languages: ");
		out.println("<ul>");
		for (String language : languages) {
			out.println("<li>" + language + "</li>");
		}
		out.println("</ul>");
		out.println("</p>");
	}

}

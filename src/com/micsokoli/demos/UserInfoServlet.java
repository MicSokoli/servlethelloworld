package com.micsokoli.demos;

import com.micsokoli.utils.HtmlUtils;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class UserInfoServlet
 */
@WebServlet("/UserInfoServlet")
public class UserInfoServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserInfoServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		
//		curl http://localhost:8080/ServletHelloWorld/UserInfoServlet -s --data "firstName=Mic" --data "lastName=Sokoli"
//		http://localhost:8080/ServletHelloWorld/UserInfoServlet?firstName=Mic&lastName=Sokoli
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		
		out.println(HtmlUtils.wrapInTag("h1", "User info"));
		out.println(HtmlUtils.wrapInTag("p", "First name: " + firstName));
		out.println(HtmlUtils.wrapInTag("p", "Last name: " + lastName));
		
		out.println("<hr>");
		
//		curl http://localhost:8080/ServletHelloWorld/UserInfoServlet -s --data "firstName=Mic" --data "lastName=Sokoli" --data "age=20"
//		http://localhost:8080/ServletHelloWorld/UserInfoServlet?firstName=Mic&lastName=Sokoli&age=20
		Enumeration<String> paramNames = request.getParameterNames();
		while (paramNames.hasMoreElements()) {
			String currentParamName = paramNames.nextElement();
			String currentParamValue = request.getParameter(currentParamName);
			out.println(HtmlUtils.wrapInTag("p", 
					"Parameter name: '" + currentParamName + "', " +
					"Parameter value: '" + currentParamValue + "'"));
		}
		
		out.println("<hr>");
		
//		curl http://localhost:8080/ServletHelloWorld/UserInfoServlet -s --data "firstName=Mic" --data "lastName=Sokoli" --data "age=20" --data "language=Albanian" --data "language=English"

		Map<String, String[]> paramMap = request.getParameterMap();
		Set<String> paramNamesSet = paramMap.keySet();
		
		for (String paramName : paramNamesSet) {
			String[] paramValues = paramMap.get(paramName);
			out.println("<div>");
			out.println(HtmlUtils.wrapInTag("p", "Parameter name: "));
			out.println("<ul><li>");
			out.println(HtmlUtils.wrapInTag("p", "'" + paramName + "'"));
			out.println("</li></ul>");
			out.println(HtmlUtils.wrapInTag("p", "Parameter values: "));
			out.println("<ul>");
			for (String paramValue : paramValues) {
				out.println("<li>");
				out.println(HtmlUtils.wrapInTag("p", "'" + paramValue + "'"));
				out.println("</li>");
			}
			out.println("</ul>");
			out.println("</div>");
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

package com.micsokoli.demos;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class DetectLocale
 */
@WebServlet("/DetectLocale")
public class DetectLocale extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public DetectLocale() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		Locale locale = request.getLocale();
		out.println("<p>locale: " + locale + "</p>");

		String language = locale.getLanguage();
		String country = locale.getCountry();
		String displayLanguage = locale.getDisplayLanguage();
		String displayCountry = locale.getDisplayCountry();
		String displayName = locale.getDisplayName();

		out.println("<p>language: " + language + "</p>");
		out.println("<p>country: " + country + "</p>");
		out.println("<p>displayLanguage: " + displayLanguage + "</p>");
		out.println("<p>displayCountry: " + displayCountry + "</p>");
		out.println("<p>displayName: " + displayName + "</p>");

		out.println("<hr>");

		Locale[] availableLocales = Locale.getAvailableLocales();
		out.println("<p>Count of available locales: " + availableLocales.length + "</p>");

		for(Locale aLocale : availableLocales) {
			out.println("<hr>");

			out.println("<p>language: " + aLocale.getLanguage() + "</p>");
			out.println("<p>country: " + aLocale.getCountry() + "</p>");
			out.println("<p>displayLanguage: " + aLocale.getDisplayLanguage() + "</p>");
			out.println("<p>displayCountry: " + aLocale.getDisplayCountry() + "</p>");
			out.println("<p>displayName: " + aLocale.getDisplayName() + "</p>");

			if( !aLocale.getScript().equals("")) {
				out.println("<p>script: " + aLocale.getScript() + "</p>");
				out.println("<p>displayScript: " + aLocale.getDisplayScript() + "</p>");
			}

			if( !aLocale.getVariant().equals("")) {
				out.println("<p>variant: " + aLocale.getVariant() + "</p>");
				out.println("<p>displayVariant: " + aLocale.getDisplayVariant() + "</p>");
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

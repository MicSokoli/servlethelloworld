package com.micsokoli.demos.trackingSessionDataHiddenFormFields;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Step2
 */
@WebServlet("/Step2")
public class Step2 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Step2() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		
		String guestName = request.getParameter("guest-name");
		
		PrintWriter out = response.getWriter();
		out.println("<!DOCTYPE html>");
		out.println("<html>");
		out.println("<head>");
		out.println("<meta charset='ISO-8859-1'>");
		out.println("<title>Guest Book</title>");
		out.println("</head>");
		out.println("<body>");
		out.println("<h1>Welcome to the guest book</h1>");
		out.println("<form action='/ServletHelloWorld/Preview' method='POST'>");
		out.println("<div>");
		out.println("<input type='hidden' name='guest-name' value='" + guestName + "'>");
		out.println("</div>");
		out.println("<div>");
		out.println("<label for='email'>Email:</label>");
		out.println("<input type='email' name='email'>");
		out.println("</div>");
		out.println("<div>");
		out.println("<button type='submit'>Preview</button>");
		out.println("</div>");
		out.println("</form>");
		out.println("</body>");
		out.println("</html>");
	}

}

package com.micsokoli.demos;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class PreviewComment
 */
@WebServlet("/PreviewComment")
public class PreviewComment extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PreviewComment() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		
		String comment = request.getParameter("comment");
		String email = request.getParameter("email");
		
		Cookie commentCookie = new Cookie("comment", comment);
		Cookie emailCookie = new Cookie("email", email);
		
		response.addCookie(commentCookie);
		response.addCookie(emailCookie);
		
		PrintWriter out = response.getWriter();
		out.println("<h3>Please review your input before submitting</h3>");
		
		out.println("<div>");
			out.println("<p>Comment: '" + comment + "'</p>");
		out.println("</div>");
		
		out.println("<div>");
			out.println("<p>Email: '" + email + "'</p>");
		out.println("</div>");
		
		out.println("<form method='POST' action='SaveComment'>");
			out.println("<button type='submit'>Save</button>");
		out.println("</form>");
		
	}

}

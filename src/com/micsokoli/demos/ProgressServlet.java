package com.micsokoli.demos;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ProgressServlet
 */
@WebServlet("/ProgressServlet")
public class ProgressServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static PrintWriter firstEverOut = null;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProgressServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out;
		if(firstEverOut == null) {
			out = response.getWriter();
			firstEverOut = out;
		} else {
			out = firstEverOut;
		}
		
		out.println("<html>");
		out.println("<head>");
		out.println("<title>Progress Servlet</title>");
		out.println("<body>");
		out.println("<p>Entering doGet(). Thread name: " + Thread.currentThread().getName() + "</p>");
		out.println("<progress id='progress' max='100'></progress>");
		int i = 0;
		while(i <= 100) {
			out.println("<script>document.getElementById('progress').value = " + i + "</script>");
			i++;
			out.flush();
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				
			}
		}
		
		out.println("</body>");
		out.println("<html>");
		
		if (i >= 100) {
			firstEverOut = null;
			out.close();
			out = null;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

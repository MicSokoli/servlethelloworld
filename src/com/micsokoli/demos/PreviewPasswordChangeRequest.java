package com.micsokoli.demos;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class PreviewPasswordChangeRequest
 */
@WebServlet("/PreviewPasswordChangeRequest")
public class PreviewPasswordChangeRequest extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PreviewPasswordChangeRequest() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		
		String email = request.getParameter("email");
		
		HttpSession session = request.getSession();
		session.setAttribute("email", email);
		
		PrintWriter out = response.getWriter();
		out.println("<h3>Please review your password change request</h3>");
		
		out.println("<div>");
			out.println("<p>Email: '" + email + "'</p>");
		out.println("</div>");
		
		out.println("<form method='POST' action='ConfirmPasswordChangeRequest'>");
			out.println("<button type='submit'>Submit</button>");
		out.println("</form>");
	}

}

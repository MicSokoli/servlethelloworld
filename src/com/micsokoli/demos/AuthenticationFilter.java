package com.micsokoli.demos;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

/**
 * Servlet Filter implementation class AuthenticationFilter
 */
@WebFilter(filterName="Authenticate", urlPatterns="/SecureServlet")
public class AuthenticationFilter implements Filter {

    /**
     * Default constructor. 
     */
    public AuthenticationFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		System.out.println("destroy() is called in " +
				this.getClass().getClass().getName());
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		System.out.println("doFilter(ServletRequest request, ServletResponse response, FilterChain chain) is called in " +
				this.getClass().getClass().getName());
		
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
		if (username.equals("user") && password.equals("guest")) {
			System.out.println("User is logged in.");
			chain.doFilter(request, response);
		} else {
			PrintWriter out = response.getWriter();
			response.setContentType("text/html");
			out.println("<h3>Sorry, you are not authorized to access this resource.</h3>");
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		System.out.println("init(FilterConfig fConfig) is called in " +
				this.getClass().getClass().getName());
	}

}

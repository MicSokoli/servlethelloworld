package com.micsokoli.demos;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class SaveComment
 */
@WebServlet("/SaveComment")
public class SaveComment extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SaveComment() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		
		Cookie[] cookies = request.getCookies();
		
		String comment = null;
		String email = null;
		
		for (Cookie cookie : cookies) {
			if (cookie.getName().equals("comment")) {
				comment = cookie.getValue();
			}
			if (cookie.getName().equals("email")) {
				email = cookie.getValue();
			}
		}
		
		PrintWriter out = response.getWriter();
		
		if (comment != null) {
			out.println("<div>");
				out.println("<p>You have submitted the comment: '" + 
					comment + "'</p>");
			out.println("</div>");
			
			if (email != null) {
				out.println("<div>");
					out.println("<p>You have given the email: '" + email + "'</p>");
				out.println("</div>");
			} else {
				out.println("<p>You haven't given any email address</p>");
			}
		} else {
			out.println("<p>You haven't submitted any comment</p>");
		}
	}

}

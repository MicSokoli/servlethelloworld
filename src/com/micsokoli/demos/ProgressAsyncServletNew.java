package com.micsokoli.demos;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ProgressAsyncServletNew
 */
@WebServlet(urlPatterns="/ProgressAsyncServletNew", asyncSupported = true)
public class ProgressAsyncServletNew extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProgressAsyncServletNew() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		out.println("<!DOCTYPE html>");
		out.println("<html>");
		out.println("<head>");
		out.println("<title>Progress Async Servlet</title>");
		out.println("</head>");
		out.println("<body>");
		out.println("<p>Entering doGet(), Thread Name: " + Thread.currentThread().getName() + "</p>");
		out.println("<progress id='progress' max='100'></progress>");
		request.startAsync();
		AsyncContext aContext = request.getAsyncContext();
		aContext.start(()->{
			out.println("<p>Executing Long Running Task using Thread: " + Thread.currentThread().getName() + "</p>");
			int i = 0;
			while (i <= 100) {
				out.println("<script>document.getElementById('progress').value = '" + i++ + "'</script>");
				out.flush();
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			out.println("<p>Completed the long running task...</p>");
			aContext.complete();
		});

		out.println("<p>Comleted the long running task</p>");
		out.println("<p>Exiting doGet(), Thread Name: " + Thread.currentThread().getName() + "</p>");
		out.println("</body>");
		out.println("</html>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

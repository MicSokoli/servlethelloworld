package com.micsokoli.demos;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Servlet implementation class Log4jDemos
 */
@WebServlet(value= {"/Log4jDemos", "/AnnotationDemo"})
public class Log4jDemos extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Logger logger = null;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Log4jDemos() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		logger = LogManager.getLogger(Log4jDemos.class);
		logger.info("Log Info: Entered the doGet Method for processing the request");
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("<!DOCTYPE html>");
		out.println("<html>");
		out.println("<head>");
		out.println("<title>Demo: Log4j 2</title>");
		out.println("</head>");
		out.println("<body>");
		out.println("<h3>Log4j 2 demo</h3>");
		logger.info("Log Info: Started the Document Creation");
		out.println("<p>Logger name: '" + logger.getName() + "'</p>");
		String param = request.getParameter("param");
		if (param == null || param.isEmpty()) {
			out.println("<p>Error: Please provide the value for the parameter</p>");
			logger.error("Error: Please provide the value for the parameter");
		} else {
			out.println("<p>Param: '" + param + "'</p>");
			logger.info("Param: '" + param + "'");
		}
		out.println("</body>");
		out.println("</html>");
		
		logger.trace("----------Why are you printing everything twice?");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
